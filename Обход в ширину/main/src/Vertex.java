class Vertex
{
    public char name; // Метка (например, 'A')
    public boolean wasVisited;

    public Vertex(char name)
    {
        this.name = name;
        wasVisited = false;
    }

}
