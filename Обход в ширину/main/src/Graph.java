public class Graph {
    private int maxN = 10;
    private Vertex vertexList[]; // Список вершин
    private int mas[][]; // Матрица смежности
    private int curN; // Текущее количество 
    private MyQueue theQueue;

    public Graph() // Конструктор
    {
        vertexList = new Vertex[maxN];
        // Матрица смежности
        mas = new int[maxN][maxN];
        curN = 0;
        for(int j=0; j<maxN; j++) // Матрица смежности
            for(int k=0; k<maxN; k++) // заполняется нулями
                mas[j][k] = 0;
        theQueue = new MyQueue();
    }

    public void addVertex(char name)
    {
        vertexList[curN++] = new Vertex(name);
    }

    public void addEdge(int start, int end)
    {
        mas[start][end] = 1;
        mas[end][start] = 1;
    }

    public void displayVertex(int v)
    {
        System.out.print(vertexList[v].name);
    }

    public void passInWidth(int index) // Обход в ширину
    { // Алгоритм начинает с вершины 0
        vertexList[index].wasVisited = true; // Пометка
        displayVertex(index); // Вывод
        theQueue.insert(index); // Вставка в конец очереди

        int vertex;
        while( !theQueue.isEmpty() ) // Пока очередь не опустеет
        {
            int temp = theQueue.remove(); // Извлечение вершины в начале очереди
            // Пока остаются непосещенные соседи
            while( (vertex=check(temp)) != -1 )
            { // Получение вершины
                vertexList[vertex].wasVisited = true; // Пометка
                displayVertex(vertex); // Вывод
                theQueue.insert(vertex); // Вставка
            }
        }
        // Очередь пуста, обход закончен
        for(int j=0; j<curN; j++) // Сброс флагов
            vertexList[j].wasVisited = false;
    }
    // -------------------------------------------------------------
    // Метод возвращает непосещенную вершину, смежную по отношению к v
    public int check(int v)
    {
        for(int j=0; j<curN; j++)
            if(mas[v][j]==1 && vertexList[j].wasVisited==false)
                return j; // Возвращает первую найденную вершину
        return -1; //  вершин нет
    }
}

