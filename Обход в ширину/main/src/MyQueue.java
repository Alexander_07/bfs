public class MyQueue {
    private int[] array;
    private int head;
    private int tail;
    private int size = 10;
    private int count;

    public MyQueue()
    {
        array = new int[size];
        head = 0;
        tail = -1;
        count = 0;
    }

    public void insert(int v) // Вставка элемента в конец очереди
    {
        if(tail == size-1)
            tail = -1;
        array[++tail] = v;
        count++;
    }

    public int remove() // Извлечение элемента в начале очереди
    {
        if(head == size)
            head = 0;

        count--;
        return array[head++];
    }

    public boolean isEmpty() // true, если очередь пуста
    {
        return count == 0;
    }
}
